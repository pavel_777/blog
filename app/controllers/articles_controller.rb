class ArticlesController < ApplicationController

	before_filter :find_article, only: [:show, :edit, :destroy ]

	http_basic_authenticate_with name: "admin", password: "admin", except: [:index, :show]
	
	def index
		@articles = Article.all
	end
	
	def show
	end

	def new
		@article = Article.new
	end

	def edit
	end
	
	def create
		@article = Article.new(article_params)
		
		if @article.save
			redirect_to article_path(@article.url)
		else
			render 'new'
		end
	end

	def update
		@article = Article.find_by_id params[:url]

		if @article.update(article_params)
			redirect_to article_path(@article.url)
		else
			render 'edit'
		end
	end

	def destroy

		@article.destroy
		
		redirect_to articles_path
	end
	
	private
	def article_params
		params.require(:article).permit(:title, :text, :url)
	end

	def find_article
		@article = Article.find_by_url params[:url]
	end
end
