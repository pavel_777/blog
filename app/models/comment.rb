class Comment < ApplicationRecord
  belongs_to :article
  validates :commenter, presence: true, length: { minimum: 5, maximum: 20}
  validates :body , presence: true, length: { minimum: 20, maximum: 100}
end
