Rails.application.routes.draw do
  get 'welcome/index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

    # resources :articles do
    # 	resources :comments
    # end

  # post 'articles_update/:id' => 'articles#update', as: 'update_post'

  # resources :articles, param: :url do
  #   resources :comments
  #   end

  resources :articles, param: :url do
    resources :comments
  end

  # patch 'articles_update/:id' => 'articles#update', as: 'updatePost'

    root 'welcome#index'
end
